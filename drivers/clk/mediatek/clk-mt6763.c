// SPDX-License-Identifier: GPL-2.0
/*
 * Copyright (c) 2022 Erik Inkinen <erik.inkinen@gmail.com>
 */

#include <linux/delay.h>
#include <linux/of.h>
#include <linux/of_address.h>
#include <linux/slab.h>
#include <linux/mfd/syscon.h>
#include <linux/regmap.h>
#include <linux/of_device.h>
#include <linux/platform_device.h>

#include "clk-mtk.h"
#include "clk-gate.h"
#include "clk-mux.h"
#include "clk-pll.h"

#include <dt-bindings/clock/mediatek,mt6763-clk.h>

#ifdef CONFIG_ARM64
#define IOMEM(a)	((void __force __iomem *)((a)))
#endif
#define mt_reg_sync_writel(v, a) \
	do { \
		__raw_writel((v), IOMEM(a)); \
		/* sync up */ \
		mb(); } \
while (0)

#define clk_readl(addr)			__raw_readl(IOMEM(addr))

#define clk_writel(addr, val)   \
	mt_reg_sync_writel(val, addr)

#define clk_setl(addr, val) \
	mt_reg_sync_writel(clk_readl(addr) | (val), addr)

#define clk_clrl(addr, val) \
	mt_reg_sync_writel(clk_readl(addr) & ~(val), addr)

#define PLL_EN  (0x1 << 0)
#define PLL_PWR_ON  (0x1 << 0)
#define PLL_ISO_EN  (0x1 << 1)

static DEFINE_SPINLOCK(mt6763_clk_lock);

/* Total 10 subsys */
void __iomem *cksys_base;
void __iomem *infracfg_base;
void __iomem *apmixed_base;
void __iomem *audio_base;
void __iomem *cam_base;
void __iomem *img_base;
void __iomem *mfgcfg_base;
void __iomem *mmsys_config_base;
void __iomem *pericfg_base;
void __iomem *venc_gcon_base;

/* CKSYS */
#define CLK_CFG_0		(cksys_base + 0x040)
#define CLK_CFG_9		(cksys_base + 0x0d0)
#define CLK_MISC_CFG_0		(cksys_base + 0x104)
#define CLK_DBG_CFG		(cksys_base + 0x10C)
#define CLK26CALI_0		(cksys_base + 0x220)
#define CLK26CALI_1		(cksys_base + 0x224)
#define CLK_SCP_CFG_0		(cksys_base + 0x0200)
#define CLK_SCP_CFG_1		(cksys_base + 0x0204)
/* CG */
#define INFRA_PDN_SET0		(infracfg_base + 0x0080)
#define INFRA_PDN_CLR0		(infracfg_base + 0x0084)
#define INFRA_PDN_STA0		(infracfg_base + 0x0090)
#define INFRA_PDN_SET1		(infracfg_base + 0x0088)
#define INFRA_PDN_CLR1		(infracfg_base + 0x008C)
#define INFRA_PDN_STA1		(infracfg_base + 0x0094)
#define INFRA_PDN_SET2		(infracfg_base + 0x00A4)
#define INFRA_PDN_CLR2		(infracfg_base + 0x00A8)
#define INFRA_PDN_STA2		(infracfg_base + 0x00AC)
#define INFRA_PDN_SET3		(infracfg_base + 0x00C0)
#define INFRA_PDN_CLR3		(infracfg_base + 0x00C4)
#define INFRA_PDN_STA3		(infracfg_base + 0x00C8)
#define INFRA_TOPAXI_SI0_CTL		(infracfg_base + 0x0200)

#define AP_PLL_CON2		(apmixed_base + 0x0008)
#define AP_PLL_CON3		(apmixed_base + 0x000C)
#define AP_PLL_CON4		(apmixed_base + 0x0010)
#define AP_PLL_CON6		(apmixed_base + 0x0018)
#define AP_PLL_CON8		(apmixed_base + 0x0020)
#define ARMPLL_LL_CON0		(apmixed_base + 0x0200)
#define ARMPLL_LL_CON1		(apmixed_base + 0x0204)
#define ARMPLL_LL_PWR_CON0	(apmixed_base + 0x020C)
#define ARMPLL_L_CON0		(apmixed_base + 0x0210)
#define ARMPLL_L_CON1		(apmixed_base + 0x0214)
#define ARMPLL_L_PWR_CON0	(apmixed_base + 0x021C)
#define MAINPLL_CON0		(apmixed_base + 0x0220)
#define MAINPLL_PWR_CON0	(apmixed_base + 0x022C)
#define UNIVPLL_CON0		(apmixed_base + 0x0230)
#define UNIVPLL_CON1		(apmixed_base + 0x0234)
#define UNIVPLL_PWR_CON0	(apmixed_base + 0x023C)
#define MFGPLL_CON0		(apmixed_base + 0x0240)
#define MFGPLL_PWR_CON0		(apmixed_base + 0x024C)
#define MSDCPLL_CON0		(apmixed_base + 0x0250)
#define MSDCPLL_PWR_CON0	(apmixed_base + 0x025C)
#define TVDPLL_CON0		(apmixed_base + 0x0260)
#define TVDPLL_PWR_CON0		(apmixed_base + 0x026C)
#define MMPLL_CON0		(apmixed_base + 0x0270)
#define MMPLL_CON1		(apmixed_base + 0x0274)
#define MMPLL_PWR_CON0		(apmixed_base + 0x027C)
#define CCIPLL_CON0		(apmixed_base + 0x0290)
#define CCIPLL_PWR_CON0		(apmixed_base + 0x029C)
#define APLL1_CON0		(apmixed_base + 0x02A0)
#define APLL1_CON1		(apmixed_base + 0x02A4)
#define APLL1_CON2		(apmixed_base + 0x02A8)
#define APLL1_PWR_CON0		(apmixed_base + 0x02B0)
#define APLL2_CON0		(apmixed_base + 0x02B4)
#define APLL2_CON1		(apmixed_base + 0x02B8)
#define APLL2_CON2		(apmixed_base + 0x02BC)
#define APLL2_PWR_CON0		(apmixed_base + 0x02C4)

#define AUDIO_TOP_CON0		(audio_base + 0x0000)
#define AUDIO_TOP_CON1		(audio_base + 0x0004)

#define CAMSYS_CG_CON		(cam_base + 0x0000)
#define CAMSYS_CG_SET		(cam_base + 0x0004)
#define CAMSYS_CG_CLR		(cam_base + 0x0008)

#define IMG_CG_CON		(img_base + 0x0000)
#define IMG_CG_SET		(img_base + 0x0004)
#define IMG_CG_CLR		(img_base + 0x0008)

#define MFG_CG_CON              (mfgcfg_base + 0x0000)
#define MFG_CG_SET              (mfgcfg_base + 0x0004)
#define MFG_CG_CLR              (mfgcfg_base + 0x0008)

#define MM_CG_CON0            (mmsys_config_base + 0x100)
#define MM_CG_SET0            (mmsys_config_base + 0x104)
#define MM_CG_CLR0            (mmsys_config_base + 0x108)
#define MM_CG_CON1            (mmsys_config_base + 0x110)
#define MM_CG_SET1            (mmsys_config_base + 0x114)
#define MM_CG_CLR1            (mmsys_config_base + 0x118)

#define VENC_CG_CON		(venc_gcon_base + 0x0000)
#define VENC_CG_SET		(venc_gcon_base + 0x0004)
#define VENC_CG_CLR		(venc_gcon_base + 0x0008)

#define INFRA_CG0 0x132f8110/*[25:24][21][19:15][8][4]*/
#define INFRA_CG1 0x080a0a64/*[27][11][9], [17][6][5][2], [19]*/
#define INFRA_CG2 0x08000001/*[0], [27]*/
#define INFRA_CG3 0x000001c7/*[6][2:0], [8:7]*/
#define CAMSYS_CG	0x1FFF
#define IMG_CG	0x3FFF
#define MFG_CG	0x1
#define MM_CG0	0xFFFFFFFF /* un-gating in preloader */
#define MM_CG1  0x000003FF /* un-gating in preloader */
#define VENC_CG 0x001111 /* inverse */



#define CK_CFG_0 0x40
#define CK_CFG_0_SET 0x44
#define CK_CFG_0_CLR 0x48
#define CK_CFG_1 0x50
#define CK_CFG_1_SET 0x54
#define CK_CFG_1_CLR 0x58
#define CK_CFG_2 0x60
#define CK_CFG_2_SET 0x64
#define CK_CFG_2_CLR 0x68
#define CK_CFG_3 0x70
#define CK_CFG_3_SET 0x74
#define CK_CFG_3_CLR 0x78
#define CK_CFG_4 0x80
#define CK_CFG_4_SET 0x84
#define CK_CFG_4_CLR 0x88
#define CK_CFG_5 0x90
#define CK_CFG_5_SET 0x94
#define CK_CFG_5_CLR 0x98
#define CK_CFG_6 0xa0
#define CK_CFG_6_SET 0xa4
#define CK_CFG_6_CLR 0xa8
#define CK_CFG_7 0xb0
#define CK_CFG_7_SET 0xb4
#define CK_CFG_7_CLR 0xb8
#define CK_CFG_8 0xc0
#define CK_CFG_8_SET 0xc4
#define CK_CFG_8_CLR 0xc8
#define CK_CFG_9 0xd0
#define CK_CFG_9_SET 0xd4
#define CK_CFG_9_CLR 0xd8
#define CK_CFG_10 0xe0
#define CK_CFG_10_SET 0xe4
#define CK_CFG_10_CLR 0xe8
#define CLK_CFG_UPDATE 0x004
#define CLK_CFG_UPDATE1 0x008

static const struct mtk_fixed_clk fixed_clks[] __initconst = {
	FIXED_CLK(CLK_TOP_CLK26M, "f_f26m_ck", "clk26m", 26000000),
};

static const struct mtk_fixed_factor top_divs[] __initconst = {
	FACTOR(CLK_TOP_CLK13M, "clk13m", "clk26m", 1,
		2),
	FACTOR(CLK_TOP_SYSPLL_CK, "syspll_ck", "mainpll", 1,
		1),
	FACTOR(CLK_TOP_SYSPLL_D2, "syspll_d2", "syspll_ck", 1,
		2),
	FACTOR(CLK_TOP_SYSPLL1_D2, "syspll1_d2", "syspll_d2", 1,
		2),
	FACTOR(CLK_TOP_SYSPLL1_D4, "syspll1_d4", "syspll_d2", 1,
		4),
	FACTOR(CLK_TOP_SYSPLL1_D8, "syspll1_d8", "syspll_d2", 1,
		8),
	FACTOR(CLK_TOP_SYSPLL1_D16, "syspll1_d16", "syspll_d2", 1,
		16),
	FACTOR(CLK_TOP_SYSPLL_D3, "syspll_d3", "mainpll", 1,
		3),
	FACTOR(CLK_TOP_SYSPLL2_D2, "syspll2_d2", "syspll_d3", 1,
		2),
	FACTOR(CLK_TOP_SYSPLL2_D4, "syspll2_d4", "syspll_d3", 1,
		4),
	FACTOR(CLK_TOP_SYSPLL2_D8, "syspll2_d8", "syspll_d3", 1,
		8),
	FACTOR(CLK_TOP_SYSPLL_D3_D3, "syspll_d3_d3", "syspll_d3", 1,
		8),
	FACTOR(CLK_TOP_SYSPLL_D5, "syspll_d5", "mainpll", 1,
		5),
	FACTOR(CLK_TOP_SYSPLL3_D2, "syspll3_d2", "syspll_d5", 1,
		2),
	FACTOR(CLK_TOP_SYSPLL3_D4, "syspll3_d4", "syspll_d5", 1,
		4),
	FACTOR(CLK_TOP_SYSPLL_D7, "syspll_d7", "mainpll", 1,
		7),
	FACTOR(CLK_TOP_SYSPLL4_D2, "syspll4_d2", "syspll_d7", 1,
		2),
	FACTOR(CLK_TOP_SYSPLL4_D4, "syspll4_d4", "syspll_d7", 1,
		4),
	FACTOR(CLK_TOP_UNIVPLL_CK, "univpll_ck", "univpll", 1,
		1),
	FACTOR(CLK_TOP_UNIVPLL_D2, "univpll_d2", "univpll_ck", 1,
		2),
	FACTOR(CLK_TOP_UNIVPLL1_D2, "univpll1_d2", "univpll_d2", 1,
		2),
	FACTOR(CLK_TOP_UNIVPLL1_D4, "univpll1_d4", "univpll_d2", 1,
		7),
	FACTOR(CLK_TOP_UNIVPLL1_D8, "univpll1_d8", "univpll_d2", 1,
		26),
	FACTOR(CLK_TOP_UNIVPLL_D3, "univpll_d3", "univpll", 1,
		3),
	FACTOR(CLK_TOP_UNIVPLL2_D2, "univpll2_d2", "univpll_d3", 1,
		2),
	FACTOR(CLK_TOP_UNIVPLL2_D4, "univpll2_d4", "univpll_d3", 1,
		4),
	FACTOR(CLK_TOP_UNIVPLL2_D8, "univpll2_d8", "univpll_d3", 1,
		8),
	FACTOR(CLK_TOP_UNIVPLL2_D16, "univpll2_d16", "univpll_d3", 1,
		16),
	FACTOR(CLK_TOP_UNIVPLL2_D32, "univpll2_d32", "univpll_d3", 1,
		32),
	FACTOR(CLK_TOP_UNIVPLL_D5, "univpll_d5", "univpll", 1,
		5),
	FACTOR(CLK_TOP_UNIVPLL3_D2, "univpll3_d2", "univpll_d5", 1,
		2),
	FACTOR(CLK_TOP_UNIVPLL3_D4, "univpll3_d4", "univpll_d5", 1,
		4),
	FACTOR(CLK_TOP_UNIVPLL3_D8, "univpll3_d8", "univpll_d5", 1,
		8),
	FACTOR(CLK_TOP_UNIVPLL_D7, "univpll_d7", "univpll", 1,
		7),
	FACTOR(CLK_TOP_UNIVPLL_D26, "univpll_d26", "univpll_ck", 1,
		26),
	FACTOR(CLK_TOP_UNIVPLL_D52, "univpll_d52", "univpll_ck", 1,
		52),
	FACTOR(CLK_TOP_UNIVPLL_D104, "univpll_d104", "univpll_ck", 1,
		104),
	FACTOR(CLK_TOP_UNIVPLL_D208, "univpll_d208", "univpll_ck", 1,
		208),
	FACTOR(CLK_TOP_APLL1_CK, "apll1_ck", "apll1", 1,
		1),
	FACTOR(CLK_TOP_APLL1_D2, "apll1_d2", "apll1", 1,
		2),
	FACTOR(CLK_TOP_APLL1_D4, "apll1_d4", "apll1", 1,
		4),
	FACTOR(CLK_TOP_APLL1_D8, "apll1_d8", "apll1", 1,
		8),
	FACTOR(CLK_TOP_APLL2_CK, "apll2_ck", "apll2", 1,
		1),
	FACTOR(CLK_TOP_APLL2_D2, "apll2_d2", "apll2", 1,
		2),
	FACTOR(CLK_TOP_APLL2_D4, "apll2_d4", "apll2", 1,
		4),
	FACTOR(CLK_TOP_APLL2_D8, "apll2_d8", "apll2", 1,
		8),
	FACTOR(CLK_TOP_TVDPLL_CK, "tvdpll_ck", "tvdpll", 1,
		1),
	FACTOR(CLK_TOP_TVDPLL_D2, "tvdpll_d2", "tvdpll_ck", 1,
		2),
	FACTOR(CLK_TOP_TVDPLL_D4, "tvdpll_d4", "tvdpll", 1,
		4),
	FACTOR(CLK_TOP_TVDPLL_D8, "tvdpll_d8", "tvdpll", 1,
		8),
	FACTOR(CLK_TOP_TVDPLL_D16, "tvdpll_d16", "tvdpll", 1,
		16),
	FACTOR(CLK_TOP_MMPLL_CK, "mmpll_ck", "mmpll", 1,
		1),
	FACTOR(CLK_TOP_MFGPLL_CK, "mfgpll_ck", "mfgpll", 1,
		1),
	FACTOR(CLK_TOP_MSDCPLL_CK, "msdcpll_ck", "msdcpll", 1,
		1),
	FACTOR(CLK_TOP_MSDCPLL_D2, "msdcpll_d2", "msdcpll", 1,
		2),
	FACTOR(CLK_TOP_MSDCPLL_D4, "msdcpll_d4", "msdcpll", 1,
		4),
	FACTOR(CLK_TOP_MSDCPLL_D8, "msdcpll_d8", "msdcpll", 1,
		8),
	FACTOR(CLK_TOP_MSDCPLL_D16, "msdcpll_d16", "msdcpll", 1,
		16),
	FACTOR(CLK_TOP_AD_OSC_CK, "ad_osc_ck", "osc", 1,
		1),
	FACTOR(CLK_TOP_OSC_D2, "osc_d2", "osc", 1,
		2),
	FACTOR(CLK_TOP_OSC_D4, "osc_d4", "osc", 1,
		4),
	FACTOR(CLK_TOP_OSC_D8, "osc_d8", "osc", 1,
		8),
	FACTOR(CLK_TOP_OSC_D16, "osc_d16", "osc", 1,
		16),
	FACTOR(CLK_TOP_OSC_D32, "osc_d32", "osc", 1,
		32),
};

static const char * const axi_parents[] __initconst = {
	"clk26m",
	"syspll1_d4",
	"syspll_d7",
	"osc_d8"
};

static const char * const mm_parents[] __initconst = {
	"clk26m",
	"mmpll_ck",
	"syspll_d3",
	"univpll1_d2",
	"syspll1_d2",
	"syspll2_d2"
};

static const char * const pwm_parents[] __initconst = {
	"clk26m",
	"univpll2_d2",
	"univpll2_d4"
};

static const char * const cam_parents[] __initconst = {
	"clk26m",
	"mmpll_ck",
	"syspll_d3",
	"univpll1_d2",
	"syspll1_d2",
	"syspll2_d2",
	"tvdpll_ck",
	"apll2_ck"
};

static const char * const mfg_parents[] __initconst = {
	"clk26m",
	"mfgpll_ck",
	"univpll_d3",
	"syspll_d3"
};

static const char * const camtg_parents[] __initconst = {
	"clk26m",
	"univpll_d52",
	"univpll2_d8",
	"univpll_d26",
	"univpll2_d16",
	"univpll2_d32",
	"univpll_d104",
	"univpll_d208"
};

static const char * const uart_parents[] __initconst = {
	"clk26m",
	"univpll2_d8"
};

static const char * const spi_parents[] __initconst = {
	"clk26m",
	"syspll3_d2",
	"syspll2_d4",
	"msdcpll_d4"
};

static const char * const msdc50_hclk_parents[] __initconst = {
	"clk26m",
	"syspll1_d2",
	"syspll2_d2"
};

static const char * const msdc50_0_parents[] __initconst = {
	"clk26m",
	"msdcpll_ck",
	"msdcpll_d2",
	"univpll1_d4",
	"syspll2_d2",
	"univpll1_d2"
};

static const char * const msdc30_1_parents[] __initconst = {
	"clk26m",
	"univpll2_d2",
	"syspll2_d2",
	"syspll_d7",
	"msdcpll_d2"
};

static const char * const msdc30_2_parents[] __initconst = {
	"clk26m",
	"univpll2_d2",
	"syspll2_d2",
	"syspll_d7",
	"msdcpll_d2"
};

/* ignore */
static const char * const msdc30_3_parents[] __initconst = {
	"clk26m",
	"msdcpll_d8",
	"msdcpll_d4",
	"univpll1_d4",
	"univpll_d26",
	"syspll_d7",
	"univpll_d7",
	"syspll3_d4",
	"msdcpll_d16"
};

static const char * const audio_parents[] __initconst = {
	"clk26m",
	"syspll3_d4",
	"syspll4_d4",
	"syspll1_d16"
};

static const char * const aud_intbus_parents[] __initconst = {
	"clk26m",
	"syspll1_d4",
	"syspll4_d2"
};

static const char * const fpwrap_ulposc_parents[] __initconst = {
	"clk26m",
	"osc_d16",
	"osc_d8",
	"osc_d32"
};

/* ignore */
static const char * const scp_parents[] __initconst = {
	"clk26m",
	"syspll1_d22",
	"syspll1_d4"
};

static const char * const atb_parents[] __initconst = {
	"clk26m",
	"syspll1_d2",
	"syspll_d5"
};

static const char * const sspm_parents[] __initconst = {
	"clk26m",
	"syspll1_d2",
	"syspll_d3"
};

static const char * const dpi0_parents[] __initconst = {
	"clk26m",
	"tvdpll_d2",
	"tvdpll_d4",
	"tvdpll_d8",
	"tvdpll_d16"
};

static const char * const scam_parents[] __initconst = {
	"clk26m",
	"syspll3_d2"
};

static const char * const aud_1_parents[] __initconst = {
	"clk26m",
	"apll1_ck"
};

static const char * const aud_2_parents[] __initconst = {
	"clk26m",
	"apll2_ck"
};

static const char * const disppwm_parents[] __initconst = {
	"clk26m",
	"univpll2_d4",
	"osc_d4",
	"osc_d16"
};

/* ignore */
static const char * const ssusb_top_sys_parents[] __initconst = {
	"clk26m",
	"univpll3_d2"
};

/* ignore */
static const char * const ssusb_top_xhci_parents[] __initconst = {
	"clk26m",
	"univpll3_d2"
};

static const char * const usb_top_parents[] __initconst = {
	"clk26m",
	"univpll3_d4"
};

static const char * const spm_parents[] __initconst = {
	"clk26m",
	"syspll1_d8"
};

static const char * const i2c_parents[] __initconst = {
	"clk26m",
	"syspll1_d8",
	"univpll3_d4"
};

static const char * const f52m_mfg_parents[] __initconst = {
	"clk26m",
	"univpll2_d2",
	"univpll2_d4",
	"univpll2_d8"
};

static const char * const seninf_parents[] __initconst = {
	"clk26m",
	"univpll_d7",
	"univpll2_d2",
	"univpll1_d4"
};

static const char * const dxcc_parents[] __initconst = {
	"clk26m",
	"syspll1_d2",
	"syspll1_d4",
	"syspll1_d8"
};

static const char * const camtg2_parents[] __initconst = {
	"clk26m",
	"univpll_d52",
	"univpll2_d8",
	"univpll_d26",
	"univpll2_d16",
	"univpll2_d32",
	"univpll_d104",
	"univpll_d208"
};

static const char * const aud_engen1_parents[] __initconst = {
	"clk26m",
	"apll1_d2",
	"apll1_d4",
	"apll1_d8"
};

static const char * const aud_engen2_parents[] __initconst = {
	"clk26m",
	"apll2_d2",
	"apll2_d4",
	"apll2_d8"
};

static const char * const faes_ufsfde_parents[] __initconst = {
	"clk26m",
	"syspll_d2",
	"syspll1_d2",
	"syspll_d3",
	"syspll1_d4",
	"univpll_d3"
};

static const char * const fufs_parents[] __initconst = {
	"clk26m",
	"syspll1_d4",
	"syspll1_d8",
	"syspll1_d16"
};


#define INVALID_UPDATE_REG 0xFFFFFFFF
#define INVALID_UPDATE_SHIFT -1
#define INVALID_MUX_GATE -1

static const struct mtk_mux top_muxes[] = {
	/* CLK_CFG_0 */
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_AXI, "axi_sel", axi_parents, CK_CFG_0,
		CK_CFG_0_SET, CK_CFG_0_CLR, 0, 2, INVALID_MUX_GATE, INVALID_UPDATE_REG, INVALID_UPDATE_SHIFT),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_MM, "mm_sel", mm_parents, CK_CFG_0,
		CK_CFG_0_SET, CK_CFG_0_CLR, 24, 3, 31, CLK_CFG_UPDATE, 3),
	/* CLK_CFG_1 */
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_PWM, "pwm_sel", pwm_parents, CK_CFG_1,
		CK_CFG_1_SET, CK_CFG_1_CLR, 0, 2, 7, CLK_CFG_UPDATE, 4),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_CAM, "cam_sel", cam_parents, CK_CFG_1,
		CK_CFG_1_SET, CK_CFG_1_CLR, 8, 3, 15, CLK_CFG_UPDATE, 5),
	/* PDN: No HS */
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_MFG, "mfg_sel", mfg_parents, CK_CFG_1,
		CK_CFG_1_SET, CK_CFG_1_CLR, 24, 2, 31, CLK_CFG_UPDATE, 7),
	/* CLK_CFG_2 */
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_CAMTG, "camtg_sel", camtg_parents, CK_CFG_2,
		CK_CFG_2_SET, CK_CFG_2_CLR, 0, 3, 7, CLK_CFG_UPDATE, 8),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_UART, "uart_sel", uart_parents, CK_CFG_2,
		CK_CFG_2_SET, CK_CFG_2_CLR, 8, 1, 15, CLK_CFG_UPDATE, 9),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_SPI, "spi_sel", spi_parents, CK_CFG_2,
		CK_CFG_2_SET, CK_CFG_2_CLR, 16, 2, 23, CLK_CFG_UPDATE, 10),
	/* CLK_CFG_3 */
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_MSDC50_0_HCLK, "msdc50_hclk_sel", msdc50_hclk_parents, CK_CFG_3,
		CK_CFG_3_SET, CK_CFG_3_CLR, 8, 2, 15, CLK_CFG_UPDATE, 12),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_MSDC50_0, "msdc50_0_sel", msdc50_0_parents, CK_CFG_3,
		CK_CFG_3_SET, CK_CFG_3_CLR, 16, 3, 23, CLK_CFG_UPDATE, 13),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_MSDC30_1, "msdc30_1_sel", msdc30_1_parents, CK_CFG_3,
		CK_CFG_3_SET, CK_CFG_3_CLR, 24, 3, 31, CLK_CFG_UPDATE, 14),
	/* CLK_CFG_4 */
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_MSDC30_2, "msdc30_2_sel", msdc30_2_parents, CK_CFG_4,
		CK_CFG_4_SET, CK_CFG_4_CLR, 0, 3, 7, CLK_CFG_UPDATE, 15),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_MSDC30_3, "msdc30_3_sel", msdc30_3_parents, CK_CFG_4,
		CK_CFG_4_SET, CK_CFG_4_CLR, 8, 4, 15, CLK_CFG_UPDATE, 16),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_AUDIO, "audio_sel", audio_parents, CK_CFG_4,
		CK_CFG_4_SET, CK_CFG_4_CLR, 16, 2, 23, CLK_CFG_UPDATE, 17),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_AUD_INTBUS, "aud_intbus_sel", aud_intbus_parents, CK_CFG_4,
		CK_CFG_4_SET, CK_CFG_4_CLR, 24, 2, 31, CLK_CFG_UPDATE, 18),
	/* CLK_CFG_5 */
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_FPWRAP_ULPOSC, "fpwrap_ulposc_sel", fpwrap_ulposc_parents, CK_CFG_5,
		CK_CFG_5_SET, CK_CFG_5_CLR, 0, 2, 7, CLK_CFG_UPDATE, 19),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_SCP, "scp_sel", scp_parents, CK_CFG_5,
		CK_CFG_5_SET, CK_CFG_5_CLR, 8, 2, 15, CLK_CFG_UPDATE, 20),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_ATB, "atb_sel", atb_parents, CK_CFG_5,
		CK_CFG_5_SET, CK_CFG_5_CLR, 16, 2, 23, CLK_CFG_UPDATE, 21),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_SSPM, "sspm_sel", sspm_parents, CK_CFG_5,
		CK_CFG_5_SET, CK_CFG_5_CLR, 24, 2, 31, CLK_CFG_UPDATE, 22),
	/* CLK_CFG_6 */
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_DPI0, "dpi0_sel", dpi0_parents, CK_CFG_6,
		CK_CFG_6_SET, CK_CFG_6_CLR, 0, 3, 7, CLK_CFG_UPDATE, 23),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_SCAM, "scam_sel", scam_parents, CK_CFG_6,
		CK_CFG_6_SET, CK_CFG_6_CLR, 8, 1, 15, CLK_CFG_UPDATE, 24),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_AUD_1, "aud_1_sel", aud_1_parents, CK_CFG_6,
		CK_CFG_6_SET, CK_CFG_6_CLR, 16, 1, 23, CLK_CFG_UPDATE, 25),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_AUD_2, "aud_2_sel", aud_2_parents, CK_CFG_6,
		CK_CFG_6_SET, CK_CFG_6_CLR, 24, 1, 31, CLK_CFG_UPDATE, 26),
	/* CLK_CFG_7 */
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_DISP_PWM, "disppwm_sel", disppwm_parents, CK_CFG_7,
		CK_CFG_7_SET, CK_CFG_7_CLR, 0, 2, 7, CLK_CFG_UPDATE, 27),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_SSUSB_TOP_SYS, "ssusb_top_sys_sel", ssusb_top_sys_parents, CK_CFG_7,
		CK_CFG_7_SET, CK_CFG_7_CLR, 8, 1, 15, CLK_CFG_UPDATE, 28),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_SSUSB_TOP_XHCI, "ssusb_top_xhci_sel", ssusb_top_xhci_parents, CK_CFG_7,
		CK_CFG_7_SET, CK_CFG_7_CLR, 16, 1, 23, CLK_CFG_UPDATE, 29),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_USB_TOP, "usb_top_sel", usb_top_parents, CK_CFG_7,
		CK_CFG_7_SET, CK_CFG_7_CLR, 24, 1, 31, CLK_CFG_UPDATE, 30),
	/* CLK_CFG_8 */
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_SPM, "spm_sel", spm_parents, CK_CFG_8,
		CK_CFG_8_SET, CK_CFG_8_CLR, 0, 1, 7, CLK_CFG_UPDATE, 31),
	/* i2c hang@8s */
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_I2C, "i2c_sel", i2c_parents, CK_CFG_8,
		CK_CFG_8_SET, CK_CFG_8_CLR, 16, 2, INVALID_MUX_GATE, CLK_CFG_UPDATE1, 1),
	/* CLK_CFG_9 */
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_F52M_MFG, "f52m_mfg_sel", f52m_mfg_parents, CK_CFG_9,
		CK_CFG_9_SET, CK_CFG_9_CLR, 0, 2, 7, CLK_CFG_UPDATE, 11),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_SENINF, "seninf_sel", seninf_parents, CK_CFG_9,
		CK_CFG_9_SET, CK_CFG_9_CLR, 8, 2, 15, CLK_CFG_UPDATE1, 3),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_DXCC, "dxcc_sel", dxcc_parents, CK_CFG_9,
		CK_CFG_9_SET, CK_CFG_9_CLR, 16, 2, 23, CLK_CFG_UPDATE1, 4),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_CAMTG2, "camtg2_sel", camtg2_parents, CK_CFG_9,
		CK_CFG_9_SET, CK_CFG_9_CLR, 24, 3, 31, CLK_CFG_UPDATE1, 9),
	/* CLK_CFG_10 */
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_AUD_ENG1, "aud_eng1_sel", aud_engen1_parents, CK_CFG_10,
		CK_CFG_10_SET, CK_CFG_10_CLR, 0, 2, 7, CLK_CFG_UPDATE1, 5),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_AUD_ENG2, "aud_eng2_sel", aud_engen2_parents, CK_CFG_10,
		CK_CFG_10_SET, CK_CFG_10_CLR, 8, 2, 15, CLK_CFG_UPDATE1, 6),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_FAES_UFSFDE, "faes_ufsfde_sel", faes_ufsfde_parents, CK_CFG_10,
		CK_CFG_10_SET, CK_CFG_10_CLR, 16, 3, 23, CLK_CFG_UPDATE1, 7),
	MUX_GATE_CLR_SET_UPD(CLK_TOP_MUX_FUFS, "fufs_sel", fufs_parents, CK_CFG_10,
		CK_CFG_10_SET, CK_CFG_10_CLR, 24, 2, 31, CLK_CFG_UPDATE1, 8),
};

static const struct mtk_gate_regs infra0_cg_regs = {
	.set_ofs = 0x80,
	.clr_ofs = 0x84,
	.sta_ofs = 0x90,
};

static const struct mtk_gate_regs infra1_cg_regs = {
	.set_ofs = 0x88,
	.clr_ofs = 0x8c,
	.sta_ofs = 0x94,
};

static const struct mtk_gate_regs infra2_cg_regs = {
	.set_ofs = 0xa4,
	.clr_ofs = 0xa8,
	.sta_ofs = 0xac,
};

static const struct mtk_gate_regs infra3_cg_regs = {
	.set_ofs = 0xc0,
	.clr_ofs = 0xc4,
	.sta_ofs = 0xc8,
};

#define GATE_INFRA0(_id, _name, _parent, _shift) {	\
		.id = _id,				\
		.name = _name,				\
		.parent_name = _parent,			\
		.regs = &infra0_cg_regs,		\
		.shift = _shift,			\
		.ops = &mtk_clk_gate_ops_setclr,	\
	}

#define GATE_INFRA1(_id, _name, _parent, _shift) {	\
		.id = _id,				\
		.name = _name,				\
		.parent_name = _parent,			\
		.regs = &infra1_cg_regs,		\
		.shift = _shift,			\
		.ops = &mtk_clk_gate_ops_setclr,	\
	}

#define GATE_INFRA2(_id, _name, _parent, _shift) {	\
		.id = _id,				\
		.name = _name,				\
		.parent_name = _parent,			\
		.regs = &infra2_cg_regs,		\
		.shift = _shift,			\
		.ops = &mtk_clk_gate_ops_setclr,	\
	}

#define GATE_INFRA3(_id, _name, _parent, _shift) {	\
		.id = _id,				\
		.name = _name,				\
		.parent_name = _parent,			\
		.regs = &infra3_cg_regs,		\
		.shift = _shift,			\
		.ops = &mtk_clk_gate_ops_setclr,	\
	}

static const struct mtk_gate infra_clks[] __initconst = {
	/* INFRA0 */
	GATE_INFRA0(CLK_INFRACFG_AO_PMIC_CG_TMR, "infra_pmic_tmr",
		"f_f26m_ck", 0),
	GATE_INFRA0(CLK_INFRACFG_AO_PMIC_CG_AP, "infra_pmic_ap",
		"f_f26m_ck", 1),
	GATE_INFRA0(CLK_INFRACFG_AO_PMIC_CG_MD, "infra_pmic_md",
		"f_f26m_ck", 2),
	GATE_INFRA0(CLK_INFRACFG_AO_PMIC_CG_CONN, "infra_pmic_conn",
		"f_f26m_ck", 3),
	GATE_INFRA0(CLK_INFRACFG_AO_SCPSYS_CG, "infra_scp",
		"axi_sel", 4),
	GATE_INFRA0(CLK_INFRACFG_AO_SEJ_CG, "infra_sej",
		"f_f26m_ck", 5),
	GATE_INFRA0(CLK_INFRACFG_AO_APXGPT_CG, "infra_apxgpt",
		"axi_sel", 6),
	GATE_INFRA0(CLK_INFRACFG_AO_ICUSB_CG, "infra_icusb",
		"usb_top_sel", 8),
	GATE_INFRA0(CLK_INFRACFG_AO_GCE_CG, "infra_gce",
		"axi_sel", 9),
	GATE_INFRA0(CLK_INFRACFG_AO_THERM_CG, "infra_therm",
		"axi_sel", 10),
	GATE_INFRA0(CLK_INFRACFG_AO_I2C0_CG, "infra_i2c0",
		"i2c_sel", 11),
	GATE_INFRA0(CLK_INFRACFG_AO_I2C1_CG, "infra_i2c1",
		"i2c_sel", 12),
	GATE_INFRA0(CLK_INFRACFG_AO_I2C2_CG, "infra_i2c2",
		"i2c_sel", 13),
	GATE_INFRA0(CLK_INFRACFG_AO_I2C3_CG, "infra_i2c3",
		"i2c_sel", 14),
	GATE_INFRA0(CLK_INFRACFG_AO_PWM_HCLK_CG, "infra_pwm_hclk",
		"i2c_sel", 15),
	GATE_INFRA0(CLK_INFRACFG_AO_PWM1_CG, "infra_pwm1",
		"i2c_sel", 16),
	GATE_INFRA0(CLK_INFRACFG_AO_PWM2_CG, "infra_pwm2",
		"i2c_sel", 17),
	GATE_INFRA0(CLK_INFRACFG_AO_PWM3_CG, "infra_pwm3",
		"i2c_sel", 18),
	GATE_INFRA0(CLK_INFRACFG_AO_PWM4_CG, "infra_pwm4",
		"i2c_sel", 19),
	GATE_INFRA0(CLK_INFRACFG_AO_PWM_CG, "infra_pwm",
		"i2c_sel", 21),
	GATE_INFRA0(CLK_INFRACFG_AO_UART0_CG, "infra_uart0",
		"uart_sel", 22),
	GATE_INFRA0(CLK_INFRACFG_AO_UART1_CG, "infra_uart1",
		"uart_sel", 23),
	GATE_INFRA0(CLK_INFRACFG_AO_UART2_CG, "infra_uart2",
		"uart_sel", 24),
	GATE_INFRA0(CLK_INFRACFG_AO_UART3_CG, "infra_uart3",
		"uart_sel", 25),
	GATE_INFRA0(CLK_INFRACFG_AO_GCE_26M_CG, "infra_gce_26m",
		"axi_sel", 27),
	GATE_INFRA0(CLK_INFRACFG_AO_CQ_DMA_FPC_CG, "infra_cqdma_fpc",
		"axi_sel", 28),
	GATE_INFRA0(CLK_INFRACFG_AO_BTIF_CG, "infra_btif",
		"axi_sel", 31),
	/* INFRA1 */
	GATE_INFRA1(CLK_INFRACFG_AO_SPI0_CG, "infra_spi0",
		"spi_sel", 1),
	GATE_INFRA1(CLK_INFRACFG_AO_MSDC0_CG, "infra_msdc0",
		"msdc50_hclk_sel", 2),
	GATE_INFRA1(CLK_INFRACFG_AO_MSDC1_CG, "infra_msdc1",
		"axi_sel", 4),
	GATE_INFRA1(CLK_INFRACFG_AO_MSDC2_CG, "infra_msdc2",
		"axi_sel", 5),
	GATE_INFRA1(CLK_INFRACFG_AO_MSDC0_SCK_CG, "infra_msdc0_sck",
		"msdc50_0_sel", 6),
	GATE_INFRA1(CLK_INFRACFG_AO_DVFSRC_CG, "infra_dvfsrc",
		"f_f26m_ck", 7),
	GATE_INFRA1(CLK_INFRACFG_AO_GCPU_CG, "infra_gcpu",
		"axi_sel", 8),
	GATE_INFRA1(CLK_INFRACFG_AO_TRNG_CG, "infra_trng",
		"axi_sel", 9),
	GATE_INFRA1(CLK_INFRACFG_AO_AUXADC_CG, "infra_auxadc",
		"f_f26m_ck", 10),
	GATE_INFRA1(CLK_INFRACFG_AO_CPUM_CG, "infra_cpum",
		"axi_sel", 11),
	GATE_INFRA1(CLK_INFRACFG_AO_CCIF1_AP_CG, "infra_ccif1_ap",
		"axi_sel", 12),
	GATE_INFRA1(CLK_INFRACFG_AO_CCIF1_MD_CG, "infra_ccif1_md",
		"axi_sel", 13),
	GATE_INFRA1(CLK_INFRACFG_AO_AUXADC_MD_CG, "infra_auxadc_md",
		"f_f26m_ck", 14),
	GATE_INFRA1(CLK_INFRACFG_AO_MSDC1_SCK_CG, "infra_msdc1_sck",
		"msdc30_1_sel", 16),
	GATE_INFRA1(CLK_INFRACFG_AO_MSDC2_SCK_CG, "infra_msdc2_sck",
		"msdc30_2_sel", 17),
	GATE_INFRA1(CLK_INFRACFG_AO_AP_DMA_CG, "infra_apdma",
		"axi_sel", 18),
	GATE_INFRA1(CLK_INFRACFG_AO_XIU_CG, "infra_xiu",
		"axi_sel", 19),
	GATE_INFRA1(CLK_INFRACFG_AO_DEVICE_APC_CG, "infra_device_apc",
		"axi_sel", 20),
	GATE_INFRA1(CLK_INFRACFG_AO_CCIF_AP_CG, "infra_ccif_ap",
		"axi_sel", 23),
	GATE_INFRA1(CLK_INFRACFG_AO_DEBUGSYS_CG, "infra_debugsys",
		"axi_sel", 24),
	GATE_INFRA1(CLK_INFRACFG_AO_AUDIO_CG, "infra_audio",
		"axi_sel", 25),
	GATE_INFRA1(CLK_INFRACFG_AO_CCIF_MD_CG, "infra_ccif_md",
		"axi_sel", 26),
	GATE_INFRA1(CLK_INFRACFG_AO_DXCC_SEC_CORE_CG, "infra_dxcc_sec_core",
		"dxcc_sel", 27),
	GATE_INFRA1(CLK_INFRACFG_AO_DXCC_AO_CG, "infra_dxcc_ao",
		"dxcc_sel", 28),
	GATE_INFRA1(CLK_INFRACFG_AO_DRAMC_F26M_CG, "infra_dramc_f26m",
		"f_f26m_ck", 31),
	/* INFRA2 */
	GATE_INFRA2(CLK_INFRACFG_AO_IRTX_CG, "infra_irtx",
		"f_f26m_ck", 0),
	GATE_INFRA2(CLK_INFRACFG_AO_DISP_PWM_CG, "infra_disppwm",
		"axi_sel", 2),
	GATE_INFRA2(CLK_INFRACFG_AO_CLDMA_BCLK_CK, "infra_cldma_bclk",
		"axi_sel", 3),
	GATE_INFRA2(CLK_INFRACFG_AO_AUDIO_26M_BCLK_CK, "infracfg_ao_audio_26m_bclk_ck",
		"f_f26m_ck", 4),
	GATE_INFRA2(CLK_INFRACFG_AO_SPI1_CG, "infra_spi1",
		"spi_sel", 6),
	GATE_INFRA2(CLK_INFRACFG_AO_I2C4_CG, "infra_i2c4",
		"i2c_sel", 7),
	GATE_INFRA2(CLK_INFRACFG_AO_MODEM_TEMP_SHARE_CG, "infra_md_tmp_share",
		"f_f26m_ck", 8),
	GATE_INFRA2(CLK_INFRACFG_AO_SPI2_CG, "infra_spi2",
		"spi_sel", 9),
	GATE_INFRA2(CLK_INFRACFG_AO_SPI3_CG, "infra_spi3",
		"spi_sel", 10),
	GATE_INFRA2(CLK_INFRACFG_AO_UNIPRO_SCK_CG, "infra_unipro_sck",
		"spi_sel", 11),
	GATE_INFRA2(CLK_INFRACFG_AO_UNIPRO_TICK_CG, "infra_unipro_tick",
		"spi_sel", 12),
	GATE_INFRA2(CLK_INFRACFG_AO_UFS_MP_SAP_BCLK_CG, "infra_ufs_mp_sap_bck",
		"spi_sel", 13),
	GATE_INFRA2(CLK_INFRACFG_AO_MD32_BCLK_CG, "infra_md32_bclk",
		"spi_sel", 14),
	GATE_INFRA2(CLK_INFRACFG_AO_SSPM_CG, "infra_sspm",
		"spi_sel", 15),
	GATE_INFRA2(CLK_INFRACFG_AO_UNIPRO_MBIST_CG, "infra_unipro_mbist",
		"spi_sel", 16),
	GATE_INFRA2(CLK_INFRACFG_AO_SSPM_BUS_HCLK_CG, "infra_sspm_bus_hclk",
		"spi_sel", 17),
	GATE_INFRA2(CLK_INFRACFG_AO_I2C5_CG, "infra_i2c5",
		"i2c_sel", 18),
	GATE_INFRA2(CLK_INFRACFG_AO_I2C5_ARBITER_CG, "infra_i2c5_arbiter",
		"i2c_sel", 19),
	GATE_INFRA2(CLK_INFRACFG_AO_I2C5_IMM_CG, "infra_i2c5_imm",
		"i2c_sel", 20),
	GATE_INFRA2(CLK_INFRACFG_AO_I2C1_ARBITER_CG, "infra_i2c1_arbiter",
		"i2c_sel", 21),
	GATE_INFRA2(CLK_INFRACFG_AO_I2C1_IMM_CG, "infra_i2c1_imm",
		"i2c_sel", 22),
	GATE_INFRA2(CLK_INFRACFG_AO_I2C2_ARBITER_CG, "infra_i2c2_arbiter",
		"i2c_sel", 23),
	GATE_INFRA2(CLK_INFRACFG_AO_I2C2_IMM_CG, "infra_i2c2_imm",
		"i2c_sel", 24),
	GATE_INFRA2(CLK_INFRACFG_AO_SPI4_CG, "infra_spi4",
		"spi_sel", 25),
	GATE_INFRA2(CLK_INFRACFG_AO_SPI5_CG, "infra_spi5",
		"spi_sel", 26),
	GATE_INFRA2(CLK_INFRACFG_AO_CQ_DMA_CG, "infra_cqdma",
		"axi_sel", 27),
	GATE_INFRA2(CLK_INFRACFG_AO_UFS_CG, "infra_ufs",
		"fufs_sel", 28),
	GATE_INFRA2(CLK_INFRACFG_AO_AES_CG, "infra_aes",
		"spi_sel", 29),
	GATE_INFRA2(CLK_INFRACFG_AO_UFS_TICK_CG, "infra_ufs_tick",
		"fufs_sel", 30),
	/* INFRA3 */
	GATE_INFRA3(CLK_INFRACFG_AO_MSDC0_SELF_CG, "infra_msdc0_self",
		"msdc50_0_sel", 0),
	GATE_INFRA3(CLK_INFRACFG_AO_MSDC1_SELF_CG, "infra_msdc1_self",
		"msdc50_0_sel", 1),
	GATE_INFRA3(CLK_INFRACFG_AO_MSDC2_SELF_CG, "infra_msdc2_self",
		"msdc50_0_sel", 2),
	GATE_INFRA3(CLK_INFRACFG_AO_SSPM_26M_SELF_CG, "infra_sspm_26m_self",
		"f_f26m_ck", 3),
	GATE_INFRA3(CLK_INFRACFG_AO_SSPM_32K_SELF_CG, "infra_sspm_32k_self",
		"f_f26m_ck", 4),
	GATE_INFRA3(CLK_INFRACFG_AO_UFS_AXI_CG, "infra_ufs_axi",
		"axi_sel", 5),
	GATE_INFRA3(CLK_INFRACFG_AO_I2C6_CG, "infra_i2c6",
		"i2c_sel", 6),
	GATE_INFRA3(CLK_INFRACFG_AO_AP_MSDC0_CG, "infra_ap_msdc0",
		"msdc50_hclk_sel", 7),
	GATE_INFRA3(CLK_INFRACFG_AO_MD_MSDC0_CG, "infra_md_msdc0",
		"msdc50_hclk_sel", 8),
};

#define MT6763_PLL_FMAX		(3200UL * MHZ)

#define CON0_MT6763_RST_BAR	BIT(24)

#define PLL_B(_id, _name, _reg, _pwr_reg, _en_mask, _flags, _pcwbits,	\
			_pd_reg, _pd_shift, _tuner_reg, _pcw_reg,	\
			_pcw_shift, _div_table) {			\
		.id = _id,						\
		.name = _name,						\
		.reg = _reg,						\
		.pwr_reg = _pwr_reg,					\
		.en_mask = _en_mask,					\
		.flags = _flags,					\
		.rst_bar_mask = CON0_MT6763_RST_BAR,			\
		.fmax = MT6763_PLL_FMAX,				\
		.pcwbits = _pcwbits,					\
		.pd_reg = _pd_reg,					\
		.pd_shift = _pd_shift,					\
		.tuner_reg = _tuner_reg,				\
		.pcw_reg = _pcw_reg,					\
		.pcw_shift = _pcw_shift,				\
		.div_table = _div_table,				\
	}

#define PLL(_id, _name, _reg, _pwr_reg, _en_mask, _flags, _pcwbits,	\
			_pd_reg, _pd_shift, _tuner_reg, _pcw_reg,	\
			_pcw_shift)					\
		PLL_B(_id, _name, _reg, _pwr_reg, _en_mask, _flags, _pcwbits, \
			_pd_reg, _pd_shift, _tuner_reg, _pcw_reg, _pcw_shift, \
			NULL)

static const struct mtk_pll_data plls[] = {
	PLL(CLK_APMIXED_MAINPLL, "mainpll", 0x0220, 0x022C, 0x00000001, HAVE_RST_BAR,
		22, 0x0224, 24, 0x0, 0x0224, 0),

	PLL(CLK_APMIXED_UNIVPLL, "univpll", 0x0230, 0x023C, 0x00000001, HAVE_RST_BAR,
		22, 0x0234, 24, 0x0, 0x0234, 0),

	PLL(CLK_APMIXED_MFGPLL, "mfgpll", 0x0240, 0x024C, 0x00000001, 0,
		22, 0x0244, 24, 0x0, 0x0244, 0),

	PLL(CLK_APMIXED_MSDCPLL, "msdcpll", 0x0250, 0x025C, 0x00000001, 0,
		22, 0x0254, 24, 0x0, 0x0254, 0),

	PLL(CLK_APMIXED_TVDPLL, "tvdpll", 0x0260, 0x026C, 0x00000001, 0,
		22, 0x0264, 24, 0x0, 0x0264, 0),

	PLL(CLK_APMIXED_MMPLL, "mmpll", 0x0270, 0x027C, 0x00000001, 0,
		22, 0x0274, 24, 0x0, 0x0274, 0),

	PLL(CLK_APMIXED_APLL1, "apll1", 0x02A0, 0x02B0, 0x00000001, 0,
		32, 0x02A0, 1, 0x0, 0x02A4, 0),

	PLL(CLK_APMIXED_APLL2, "apll2", 0x02b4, 0x02c4, 0x00000001, 0,
		32, 0x02b4, 1, 0x0, 0x02b8, 0),
};

static int __init clk_mt6763_apmixed_probe(struct platform_device *pdev)
{
	struct clk_hw_onecell_data *clk_data;
	int r;
	struct device_node *node = pdev->dev.of_node;
	void __iomem *base;
	struct resource *res = platform_get_resource(pdev, IORESOURCE_MEM, 0);

	base = devm_ioremap_resource(&pdev->dev, res);
	if (IS_ERR(base)) {
		pr_err("%s(): ioremap failed\n", __func__);
		return PTR_ERR(base);
	}

	clk_data = mtk_alloc_clk_data(CLK_APMIXED_NR_CLK);

	mtk_clk_register_plls(node, plls, ARRAY_SIZE(plls), clk_data);

	r = of_clk_add_hw_provider(node, of_clk_hw_onecell_get, clk_data);

	if (r)
		pr_err("%s(): could not register clock provider: %d\n",
			__func__, r);
	
	apmixed_base = base;
	
	clk_writel(AP_PLL_CON3, clk_readl(AP_PLL_CON3) & 0x007d5550);/* MPLL, CCIPLL, MAINPLL, TDCLKSQ, CLKSQ1 */
	clk_writel(AP_PLL_CON4, clk_readl(AP_PLL_CON4) & 0x7f5);
	clk_writel(AP_PLL_CON6, clk_readl(AP_PLL_CON6) & 0xfffdffff);/* [17] = 0 */
	clk_writel(AP_PLL_CON8, clk_readl(AP_PLL_CON8) & 0xfffff6af);/*[4]SSUSB26M, [11][6]MIPIC camera, [8] mm26m*/

/*UNIVPLL*/
	clk_clrl(UNIVPLL_CON0, PLL_EN);
	clk_setl(UNIVPLL_PWR_CON0, PLL_ISO_EN);
	clk_clrl(UNIVPLL_PWR_CON0, PLL_PWR_ON);
/*MSDCPLL*/
	clk_clrl(MSDCPLL_CON0, PLL_EN);
	clk_setl(MSDCPLL_PWR_CON0, PLL_ISO_EN);
	clk_clrl(MSDCPLL_PWR_CON0, PLL_PWR_ON);
	
/*TVDPLL*/
	clk_clrl(TVDPLL_CON0, PLL_EN);
	clk_setl(TVDPLL_PWR_CON0, PLL_ISO_EN);
	clk_clrl(TVDPLL_PWR_CON0, PLL_PWR_ON);
/*APLL1*/
	clk_clrl(APLL1_CON0, PLL_EN);
	clk_setl(APLL1_PWR_CON0, PLL_ISO_EN);
	clk_clrl(APLL1_PWR_CON0, PLL_PWR_ON);
/*APLL2*/
	clk_clrl(APLL2_CON0, PLL_EN);
	clk_setl(APLL2_PWR_CON0, PLL_ISO_EN);
	clk_clrl(APLL2_PWR_CON0, PLL_PWR_ON);

	return r;
}

static int __init clk_mt6763_top_probe(struct platform_device *pdev)
{
	int r;
	struct device_node *node = pdev->dev.of_node;
	void __iomem *base;
	struct clk_hw_onecell_data *clk_data;
	struct resource *res = platform_get_resource(pdev, IORESOURCE_MEM, 0);

	base = devm_ioremap_resource(&pdev->dev, res);
	if (IS_ERR(base)) {
		pr_err("%s(): ioremap failed\n", __func__);
		return PTR_ERR(base);
	}

	clk_data = mtk_alloc_clk_data(CLK_TOP_NR_CLK);

	mtk_clk_register_fixed_clks(fixed_clks, ARRAY_SIZE(fixed_clks),
					clk_data);
	mtk_clk_register_factors(top_divs, ARRAY_SIZE(top_divs), 
					clk_data);
	mtk_clk_register_muxes(top_muxes, ARRAY_SIZE(top_muxes), node,
			       	&mt6763_clk_lock, clk_data);

	r = of_clk_add_hw_provider(node, of_clk_hw_onecell_get, clk_data);

	if (r)
		pr_err("%s(): could not register clock provider: %d\n",
			__func__, r);

	cksys_base = base;

	clk_writel(CLK_SCP_CFG_0, clk_readl(CLK_SCP_CFG_0) | 0x3FF);
	clk_writel(CLK_SCP_CFG_1, clk_readl(CLK_SCP_CFG_1) | 0x11);

	/* PWM7, MFG31 MUX PDN */
	clk_writel(cksys_base + CK_CFG_1_CLR, 0x00000080);
	clk_writel(cksys_base + CK_CFG_1_SET, 0x00000080);

	/* msdc50_0_hclk15, msdc50_023 MUX PDN */
	clk_writel(cksys_base + CK_CFG_3_CLR, 0x00808000);
	clk_writel(cksys_base + CK_CFG_3_SET, 0x00808000);

	/* msdc30_2 7, msdc30_3 15 MUX PDN */
	clk_writel(cksys_base + CK_CFG_4_CLR, 0x00008080);
	clk_writel(cksys_base + CK_CFG_4_SET, 0x00008080);

	/* scp15, atb23 MUX PDN */
	clk_writel(cksys_base + CK_CFG_5_CLR, 0x00808000);
	clk_writel(cksys_base + CK_CFG_5_SET, 0x00808000);

	/* dpi0 7, scam 15 MUX PDN */
	clk_writel(cksys_base + CK_CFG_6_CLR, 0x00008080);
	clk_writel(cksys_base + CK_CFG_6_SET, 0x00008080);

	/* ssusb_top_sys 15, ssusb_top_xhci 23 MUX PDN */
	clk_writel(cksys_base + CK_CFG_7_CLR, 0x00808000);
	clk_writel(cksys_base + CK_CFG_7_SET, 0x00808000);
	
	return r;
}

static int __init clk_mt6763_infracfg_ao_probe(struct platform_device *pdev)
{
	struct clk_hw_onecell_data *clk_data;
	int r;
	struct device_node *node = pdev->dev.of_node;
	void __iomem *base;
	struct resource *res = platform_get_resource(pdev, IORESOURCE_MEM, 0);

	base = devm_ioremap_resource(&pdev->dev, res);
	if (IS_ERR(base)) {
		pr_err("%s(): ioremap failed\n", __func__);
		return PTR_ERR(base);
	}

	clk_data = mtk_alloc_clk_data(CLK_INFRACFG_AO_NR_CLK);

	mtk_clk_register_gates(node, infra_clks, ARRAY_SIZE(infra_clks), 
					clk_data);

	r = of_clk_add_hw_provider(node, of_clk_hw_onecell_get, clk_data);

	if (r)
		pr_err("%s(): could not register clock provider: %d\n",
			__func__, r);

	infracfg_base = base;
	clk_writel(INFRA_TOPAXI_SI0_CTL, clk_readl(INFRA_TOPAXI_SI0_CTL) | 0x2);
	pr_err("%s: infra mfg debug: %08x\n",
			__func__, clk_readl(INFRA_TOPAXI_SI0_CTL));
			
	clk_writel(INFRA_PDN_SET0, INFRA_CG0);
	clk_writel(INFRA_PDN_SET1, INFRA_CG1);
	clk_writel(INFRA_PDN_SET2, INFRA_CG2);
	clk_writel(INFRA_PDN_SET3, INFRA_CG3);

	return r;
}

static const struct of_device_id of_match_clk_mt6763[] = {
	{
		.compatible = "mediatek,mt6763-apmixedsys",
		.data = clk_mt6763_apmixed_probe,
	}, {
		.compatible = "mediatek,mt6763-topckgen",
		.data = clk_mt6763_top_probe,
	}, {
		.compatible = "mediatek,mt6763-infracfg_ao",
		.data = clk_mt6763_infracfg_ao_probe,
	}, {
		/* sentinel */
	}
};

static int clk_mt6763_probe(struct platform_device *pdev)
{
	int (*clk_probe)(struct platform_device *d);
	int r;

	clk_probe = of_device_get_match_data(&pdev->dev);
	if (!clk_probe)
		return -EINVAL;

	r = clk_probe(pdev);
	if (r)
		dev_err(&pdev->dev,
			"could not register clock provider: %s: %d\n",
			pdev->name, r);

	return r;
}

static struct platform_driver clk_mt6763_drv = {
	.probe = clk_mt6763_probe,
	.driver = {
		.name = "clk-mt6763",
		.of_match_table = of_match_clk_mt6763,
	},
};

static int __init clk_mt6763_init(void)
{
	return platform_driver_register(&clk_mt6763_drv);
}

arch_initcall(clk_mt6763_init);

